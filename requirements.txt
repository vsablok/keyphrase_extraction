
Flask==1.1.2
Flask-Cors==3.0.10
kex==2.0.5
sentence_transformers==2.1.0
tokenizers==0.10.3
keybert==0.5.0
pandas==1.2.2
spacy==3.1.3
https://github.com/explosion/spacy-models/releases/download/en_core_web_sm-3.1.0/en_core_web_sm-3.1.0-py3-none-any.whl
https://github.com/explosion/spacy-models/releases/download/en_core_web_trf-3.1.0/en_core_web_trf-3.1.0-py3-none-any.whl
https://intel-optimized-pytorch.s3.cn-north-1.amazonaws.com.cn/wheels/v1.9/torch_ipex-1.9.0-cp37-cp37m-linux_x86_64.whl