from flask import Flask, request
from getFunctions import *

application = Flask(__name__)

@application.route('/getkeywords',methods=['POST'])
def getkeywords():
    try:
        text = request.get_json().get('comment')
    except:
        return json.dumps({'result': {"keywords": []},"version": "1.0","status": "failure","code": 400,"message": "Bad Request - Could not extract comment from the request body."})
    if text is None or text=="":
        return json.dumps({'result': {"keywords": []},"version": "1.0","status": "failure","code": 400,"message": "Comment is Empty."})
    else:
        keywords=get_keywords(text)[0:5]
        return json.dumps({'result': {"keywords": keywords},"version": "1.0","status": "success","code": 200,"message": ""})