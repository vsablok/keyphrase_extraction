**This API will be used to extract keywords from Employee Feedback.**

Here's an example -

Comment - I enjoyed the people I worked with and my job role. There was a lot of turnover, and management was not interested in keeping good people. While I enjoyed the work, there were no bonuses or even employee reward programs for accomplishments. Lack of employee empathy drained morale.

API Output - [ "drain morale", "employee reward program", "job role", "management", "turnover" ]

---

## Execution Flow

This application will be running on an Elastic Beanstalk Environment(Python 3.7). Following steps were taken to make this code compatible with Elastic Beanstalk -

1. Application name was changed to 'Application.py' so EB environment can pick it up automatically.
2. Application.run was removed from the Application as Gunicorn takes care of launching the API in the EB Environment on Port 80 by default.
3. dependencies.txt file was added so EB can automatically detect and install these dependencies via PIP.
4. We will need to add this repository to a zip File so we can upload it to EB.

---